/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop_ca2;

/**
 *
 * @author Jordan
 */
public enum SortType {
    ASCENDING(1),
    DESCENDING(-1);

    private int sortType = 1;

    private SortType(int sortType) {
        this.sortType = sortType;
    }

    public int getValue() {
        return sortType;
    }
}
