
package oop_ca2;

/**
 * OOP_CA2 SEMESTER 2 GROUP PROJECT
 * @author Jordan and Conor
 */
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;

/*  
                    •	score (value indicating the quality of match for search query)
                    •	queryName (query name used to retrieve this Actor)
                    •	name
                    •	id
                    •	imageUrls (2 or more)
                    •	personLink
                    •	myRating 
                    •	myComments
*/

public class Main
{
    private static String url = "http://api.tvmaze.com/search/people?q=";
    private static String fileName = "person.dat";
    private static Scanner in = new Scanner(System.in);
    private static String searchTerm = "";
    //Below are the padding and depadding number for both load() and save() files
    private static int queryNamePad = 30;
    private static int namePad = 30;
    private static int imageUrlsPad = 80;
    private static int personLinkPad = 70;
    private static int myCommentsPad = 40;
    
    
    public static void main(String[] args) throws IOException
    {
        System.out.println("Welcome to Jordan and Conor's Actorage. The place for all your actor searching needs!\n");
        Map<String, List<Person>> persons = load();//load();
        ArrayList<Person> personList = new ArrayList<>();
        ArrayList<Person> sortArray = new ArrayList<>();
        // -> Swtich Statement
        int switchOpt = 0;
        while(switchOpt !=7)
            {
                System.out.println("Select an option:"
                        + "\n1: Search for person"
                        + "\n2: List all persons"
                        + "\n3: List all persons by id"
                        + "\n4: List persons by rating"
                        + "\n5: Edit person" 
                        + "\n6: Export search results to HTML file"
                        + "\n7: Save and Exit");

                System.out.print("\nSelect: ");
                switchOpt = in.nextInt();               
                switch(switchOpt)
                {
                    case 1: // -> search for person
                        in.nextLine();
                        System.out.println("Please enter search term for tvmaze:");
                        searchTerm = in.nextLine().toLowerCase();
                        System.out.println("\nProcessing....");
                        if(searchPersonInMap(searchTerm, persons) == false)
                        {
                            System.out.println("Query name not found in Map... Searchng Api for results.\n");
                            searchPersonInApi(searchTerm, persons);
                        }
                        break;
                    case 2: // -> print persons
                        System.out.println("");
                        personList = convertToArrayList(persons);
                        sortArray = sort(personList, "score");
                        printPersons(sortArray);  
                        //printPersons(persons);
                        break;
                    case 3:
                   // personList = new ArrayList<>(persons.values());
                    personList = convertToArrayList(persons);
                    getAscDesc();
                    sortArray = sort(personList, "id");
                    printPersons(sortArray);              
                        break;
                    case 4:
                        
                    //personList = new ArrayList<>(persons.values());
                    personList = convertToArrayList(persons);
                    getAscDesc();
                    sortArray = sort(personList, "rating");
                    printPersons(sortArray);                
                        break;
                    case 5:
                        editPerson(persons);
                        break;
                    case 6:
                        writeToHtmlFile(persons);
                        System.out.println("\nEntries added to HTML file");
                        break;
                    case 7:
                        System.out.println("Writing file....");
                        save(persons);
                        System.out.println("File complete. Exiting Application");
                        break;
                }
            }
    }
    
    public static ArrayList<Person> convertToArrayList(Map<String, List<Person>> persons)
    {
        ArrayList<Person> personList = new ArrayList<>();
        for(Map.Entry<String, List<Person>> entry: persons.entrySet())
        {
            for(Person x: entry.getValue())
            {
                personList.add(x);
            }
        }
        
        return personList;
    }
    
    public static boolean searchPersonInMap(String searchTerm, Map<String, List<Person>> persons)
    {
        if (persons.containsKey(searchTerm)) 
        {
            System.out.println("Query name found in Map... Printing\n");
            List<Person> value = persons.get(searchTerm);
            for(Person x : value)
            {
                System.out.println("Key: " + searchTerm);
                System.out.println(x);
            }
            return true;
        }
        return false;
    }
    
    public static Map<String, List<Person>> searchPersonInApi(String searchTerm, Map<String, List<Person>> persons)
    {
        //private static String url = "http://api.tvmaze.com/search/people?q=";
        try 
        {
            List<Person> p = new ArrayList<>();
            String encode = URLEncoder.encode(searchTerm, "UTF-8"); //encode the data
            URL website = new URL(url+encode);
            InputStream siteIn = website.openStream(); //get the info from site
            JsonReader reader = Json.createReader(siteIn); //Creates a JSON reader from a character stream.
            JsonArray allResults = reader.readArray(); //Returns a JSON array that is represented in the input source. 
            //This method needs to be called only once for a reader instance. 
            for(int i = 0; i < allResults.size(); i++)
            {
                JsonObject obj = allResults.getJsonObject(i);
                JsonObject person = obj.getJsonObject("person");
                JsonValue ratingValue = person.get("id"); 

                List<String> images = new ArrayList<>(); // -> Stores images
                List<String> myComments = new ArrayList<>(); // -> Stores my comments
                JsonValue objScore = obj.get("score");
                double score = ((JsonNumber)objScore).doubleValue();// -> Score
                String name = person.getString("name"); // -> Name
                // -> Id 
                int id;
                if(ratingValue instanceof JsonNumber)
                {
                    id = ((JsonNumber)ratingValue).intValue();
                }
                else 
                {
                    id = 0;
                }
                // -> ImageUrls
                String original, medium;
                if(!obj.getJsonObject("person").isNull("image"))
                {
                    JsonObject imageUrls = person.getJsonObject("image");
                    medium = imageUrls.getString("medium");
                    original = imageUrls.getString("original");
                }
                else
                {
                    medium = "No medium image";
                    original = "No original image";
                }
                images.add(medium);
                images.add(original);
                String personLink = person.getString("url"); // -> PersonLink
                double myRating = 0; // -> MyRating

                p.add(new Person(score, searchTerm, name, id, images, personLink, myRating, myComments)); // -> Add person to Map
            }
            persons.put(searchTerm, p);
            for(Person pf: p)
            {
                System.out.println("Key: " + searchTerm);
                System.out.println(pf);
            }
                return persons;
        }
        catch (UnsupportedEncodingException ex) 
        {
            Logger.getLogger(OOP_CA2.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (MalformedURLException ex) 
        {
            Logger.getLogger(OOP_CA2.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(OOP_CA2.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Added Sucessfully!");
        return persons;
    }
    
    public static void printPersons(ArrayList<Person> sortArray)
    {
         for (Person a : sortArray) 
            {
                System.out.println("Key: " + a.getQueryName());
                System.out.println(a);
            }
    }
    
    public static ArrayList<Person> sort(ArrayList<Person> personList, String sortingMethod)
    {
        ArrayList<Person> cloneList = clone(personList);
        Comparator c = SortFactory.getComparator(sortingMethod);
        
        Collections.sort(cloneList, c);
        return cloneList;
    }
    
    public static void getAscDesc()
    {
        Scanner in = new Scanner(System.in);
        boolean typeFound = false;
        System.out.println("1: Ascending\n2: Descending");
        int type = in.nextInt();       
        while(!typeFound){
            if(type == 1){
                SortFactory.sortType = SortType.ASCENDING;
                typeFound = true;
            }
            else if (type == 2){
                SortFactory.sortType = SortType.DESCENDING;
                typeFound = true;
            } 
            else
            {
                System.out.println("INVALID INPUT. Try again");
                type = in.nextInt();
            }
        }
    }
    
    public static ArrayList<Person> clone(ArrayList<Person> personList)
    {
        ArrayList<Person> tempList = new ArrayList<Person>();
        for(Person m : personList)
        {
            tempList.add(m.clone());
        }
        return tempList;
    }
    
    public static void editPerson(Map<String, List<Person>> persons)
    {
        in.nextLine();
        System.out.println("What person would you like to edit? (by key)");
        String search = in.nextLine().toLowerCase();
        if(persons.containsKey(search))
        {
            int pos = 0;
            List<Person> value = persons.get(search);
            System.out.println();
            for(Person x : value)
            {
                System.out.println("Position: " + pos + "  Name: " + x.getName() + "  ID: " + x.getId());
                pos++;
            }
            
            System.out.println("\nWhich person would you like to edit? (by Postion)");
            int searchId = in.nextInt();
            while(searchId < 0 || searchId > value.size())
            {
                System.out.print("  <-- That person doesn't exist. Try again\n");
                searchId = in.nextInt();
            }
            in.nextLine();
            
            System.out.println("\nWould you like to add a 'comment' or a 'rating'? to " + value.get(searchId).getName());
            String searchT = in.nextLine();
            if(searchT.equalsIgnoreCase("comment"))
            {
                System.out.print("\nComment: ");
                String comment = in.nextLine();
                value.get(searchId).addComments(comment);

            }
            else if(searchT.equalsIgnoreCase("rating"))
            {
                System.out.print("\nRating:");
                double rating = in.nextDouble();
                while(rating < 0 || rating > 5)
                {
                    System.out.println("Invalid Rating Amount, your rating must be on a scale of 0 - 5");
                    System.out.print("\nRating: ");
                    rating = in.nextDouble();
                }
                value.get(searchId).setMyRating(rating);
            }
            System.out.println("Update Successful!\n");
        }
        else
        {
            System.out.println("\nOops, look's like the person your looking for doesn't go by the key \"" + search + "\"");
            System.out.println("Make sure the key matches the person you want to edit");
            System.out.println("If you can't find the person, select 2 to see if the person exists");
            System.out.println("If the person does not exist, select 1 and we'll find him for you\n");
        }
        
    }
    
    public static void writeToHtmlFile(Map<String, List<Person>> persons) throws IOException
    {
        BufferedWriter bw = new BufferedWriter(new FileWriter("persons.html"));
        bw.write("<html>\n");
        bw.write("<head>\n<link rel='stylesheet' type='text/css' href='html.css'>\n<title>Exported Html File</title>\n</head>\n");
        bw.write("<body>\n");
        for(Map.Entry<String, List<Person>> entry: persons.entrySet())
        {
            List<Person> list = entry.getValue();
            for (Person value : list)
            {
                bw.write("<div id='main'>\n");
                String name = value.getName();
                bw.write("<h2>" + name + "</h2>\n");
                double score = value.getScore();
                bw.write("Score: " + score + "<br>");
                String queryName = value.getQueryName();
                bw.write("Query Name: " + queryName + "<br>");
                int id = value.getId();
                bw.write("ID: " + id + "<br>");
                bw.write("Images: ");
                List<String> images = value.getImageUrls(); // -> images
                bw.write("<div id='image'><img src='" + images.get(0) + "' alt='image'></div>");
                bw.write("<a href='" + images.get(1) + "'>View original image</a>");
                bw.write("<br>");
                String personLink = value.getPersonLink(); // -> personLink
                bw.write("Person Link: <a href='" + personLink + "'>Click Here</a><br>");
                bw.write("My Comments: ");
                List<String> myComments = value.getMyComments(); // -> myComments
                if(!myComments.isEmpty())
                {
                    for (int i = 0; i < myComments.size(); i++) 
                    {
                        bw.write("\"" + myComments.get(i) + "\"");
                        if (i < myComments.size()-1)
                        {
                            bw.write(", ");
                        }
                    }
                }
                else 
                {
                    bw.write("No comments found");
                }
                bw.write("<br>");
                double myRating = value.getMyRating();
                bw.write("My Rating: " + myRating + "<br><br>\n");
                bw.write("</div>\n");
            }
        }
        bw.write("</body>\n");
        bw.write("</html>");
        bw.close();
    }
    
    private static void save(Map<String, List<Person>> persons)
    {
        try
        {
            ArrayList<Person> p = convertToArrayList(persons);
            
            File f = new File(fileName);
            if(!f.exists())
            {
                f.createNewFile();
            }
            
            DataOutputStream dos = new DataOutputStream(new FileOutputStream(f));
            Iterator<Person> iter = p.iterator();
            while(iter.hasNext())
            {
                Person s = iter.next();
                dos.writeDouble(s.getScore()); // -> score
                dos.writeBytes(pad(s.getQueryName(), queryNamePad)); // -> queryName
                dos.writeBytes(pad(s.getName(), namePad)); // -> name
                dos.writeInt(s.getId()); // -> id
                List<String> images = s.getImageUrls(); // -> images
                for (String string : images) 
                {
                    dos.writeBytes(pad(string, imageUrlsPad));
                }
                dos.writeBytes(pad(s.getPersonLink(), personLinkPad)); // -> personLink
                dos.writeDouble(s.getMyRating()); // -> myRating
                //System.out.println(s.getMyComments());
                List<String> comments = s.getMyComments();
                //System.out.println(comments.size());// -> myComments
                int commentCount = comments.size();
                dos.writeInt(commentCount);
                for (String string : comments) 
                {
                    dos.writeBytes(pad(string, myCommentsPad));
                }
            }
            dos.close();
        } 
        catch (IOException ex) 
        {
            ex.printStackTrace();
        }
    }
    
    public static Map<String, List<Person>> load() throws FileNotFoundException, IOException
    {
       File f = new File(fileName);
       Map<String, List<Person>> persons = new HashMap<>();
       List<Person> list = new ArrayList<>();
       if(f.exists())
       {
           DataInputStream dis = new DataInputStream(new FileInputStream(f));
           int commentCount = 0;
           while(dis.available() > 0)
           {
               //System.out.println(dis.available());
               double score = dis.readDouble();
               ///System.out.println(score);
               
               byte[] queryNameBytes = new byte[queryNamePad];
               dis.read(queryNameBytes);
               String queryName = removePadding(new String(queryNameBytes));
               //System.out.println(queryName);
                   
               byte[] nameBytes = new byte[namePad];
               dis.read(nameBytes);
               String name = removePadding(new String(nameBytes));
               //System.out.println(name);
               
               int id = dis.readInt();
               //System.out.println(id);
               
               List<String> imageUrls = new ArrayList<>();
               
               byte[] mediumImage = new byte[imageUrlsPad];
               dis.read(mediumImage);
               String mImage = removePadding(new String(mediumImage));
               imageUrls.add(mImage);
               
               byte[] originalImage = new byte[imageUrlsPad];
               dis.read(originalImage);
               String oImage = removePadding(new String(originalImage));
               imageUrls.add(oImage);
               //System.out.println(imageUrls);
               
               byte[] personLinkBytes = new byte[personLinkPad];
               dis.read(personLinkBytes);
               String personLink = removePadding(new String(personLinkBytes));
               //System.out.println(personLink);
               
               double myRating = dis.readDouble();
               //System.out.println(myRating);
               
               List<String> myComments = new ArrayList<>();
               
               commentCount = dis.readInt();
               //System.out.println(commentCount);
               for(int i = 0; i < commentCount; i++)
               {
                    byte[] myCommentsBytes = new byte[myCommentsPad];
                    dis.read(myCommentsBytes);
                    String myComment = removePadding(new String(myCommentsBytes));
                    myComments.add(myComment);
               }
               // at this point i have all info about one person
               // need to check if queryname exists and add the info to the list
               if(!persons.containsKey(queryName))
               {   
                   list = new ArrayList<>(); //need to declare a new arraylist once a new queryName has been found
                   persons.put(queryName, list);
               }
               if(persons.containsKey(queryName))
               {
                    list.add(new Person(score, queryName, name, id, imageUrls, personLink, myRating, myComments)); // -> Add person to Map
               }
               
           }
           dis.close();
       }
       return persons;
    }
    
    public static String pad(String s, int size)
    {
        while (s.length() < size)
        {
            s = (char)0 + s;
        }
        return s;
    }
    
    public static String removePadding(String s)
    {
        while(s.charAt(0) == (char)0)
        {
            s = s.substring(1);
        }
        return s;
    }
    
}



