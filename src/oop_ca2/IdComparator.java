/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop_ca2;

import java.util.Comparator;

/**
 *
 * @author Jordan
 */
public class IdComparator implements Comparator<Person>
{
    
    private final int sortType;
    public IdComparator(SortType sortType)
    {
        this.sortType = sortType.getValue();
    }
    
    @Override
    public int compare(Person o1, Person o2) 
    {
        if(o1.getId() > o2.getId())
        {
            return 1 * sortType;
        }
        return -1 * sortType;
    }
    
}
