
package oop_ca2;

/**
 * NOTE: THIS FILE IS A TEST, PLEASE USE 'Main.java'
 *
 * @author Jordan and Conor
 */
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;

/*  
                    •	score (value indicating the quality of match for search query)
                    •	queryName (query name used to retrieve this Actor)
                    •	name
                    •	id
                    •	imageUrls (2 or more)
                    •	personLink
                    •	myRating 
                    •	myComments
*/

public class OOP_CA2 
{
    private static String url = "http://api.tvmaze.com/search/people?q=";
    private static Scanner in = new Scanner(System.in);
    public static void main(String[] args) throws IOException
    {
        System.out.println("Welcome to Jordan and Conors Actorage");
        Map<Integer, Person> persons = load(); //load();
        ArrayList<Person> personList = new ArrayList<>();
        ArrayList<Person> sortArray = new ArrayList<>();
        // -> Swtich Statement
        int switchOpt = 0;
        while(switchOpt !=7)
            {
                System.out.println("\nSelect an option:"
                        + "\n1: Search for person"
                        + "\n2: List all persons"
                        + "\n3: List all persons by id"
                        + "\n4: List persons by rating"
                        + "\n5: Edit person" 
                        + "\n6: Export search results to HTML file"
                        + "\n7: Save and Exit");

                System.out.print("\nSelect: ");
                switchOpt = in.nextInt();               
                switch(switchOpt)
                {
                    case 1: // -> search for person
                        in.nextLine();
                        System.out.println("Please enter search term for tvmaze:");
                        String searchTerm = in.nextLine().toLowerCase();
                        System.out.println("\nProcessing....");
                        persons = searchPersonInMap(searchTerm, persons);
                        break;
                    case 2: // -> print persons
                        System.out.println("");
                        personList = convertToArrayList(persons);
                        sortArray = sort(personList, "score");
                        printPersons(sortArray);  
                        //printPersons(persons);
                        break;
                    case 3:
                   // personList = new ArrayList<>(persons.values());
                    personList = convertToArrayList(persons);
                    getAscDesc();
                    sortArray = sort(personList, "id");
                    printPersons(sortArray);              
                        break;
                    case 4:
                        
                    //personList = new ArrayList<>(persons.values());
                    personList = convertToArrayList(persons);
                    getAscDesc();
                    sortArray = sort(personList, "rating");
                    printPersons(sortArray);                
                        break;
                    case 5:
                        editPerson(persons);
                        break;
                    case 6:
                        writeToHtmlFile(persons);
                        System.out.println("\nEntries added to HTML file");
                        break;
                    case 7:
                        System.out.println("Writing file....");
                        save(persons);
                        System.out.println("File complete. Exiting Application");
                        break;
                }
            }
    }
    
    public static ArrayList<Person> convertToArrayList(Map<Integer, Person> persons)
    {
        ArrayList<Person> personList = new ArrayList<>();
        for(Map.Entry<Integer, Person> entry: persons.entrySet())
        {
            int key = entry.getKey();
            Person value = entry.getValue();
            personList.add(new Person(value.getScore(), value.getQueryName(), value.getName(), value.getId(), value.getImageUrls(), value.getPersonLink(), value.getMyRating(), value.getMyComments()));
        }
        
        return personList;
    }
    
    public static Integer printSearchResult(Map<Integer, Person> persons, String searchTerm, int count)
    {
        for(Map.Entry<Integer, Person> entry: persons.entrySet()) //prints results found by user
        {
            Person value = entry.getValue();
            if(value.getQueryName().equalsIgnoreCase(searchTerm))
            {
                System.out.println(value);
                count++;
            }  
        }
        return count;
    }
    
    public static Map<Integer, Person> searchPersonInMap(String searchTerm, Map<Integer, Person> persons)
    {
        int count = 0;
        
        count = printSearchResult(persons, searchTerm, count);
        
        if(count == 0) //if no matches are found, we then run the tv maze api
        {
            System.out.println("Actor was not found in Map, searching API for results...\n");
            searchPersonInApi(searchTerm, persons);
        }
        else
        {
            System.out.println(count + " actors found in map");
        }
        return persons;
    }
    
    public static Map<Integer, Person> searchPersonInApi(String searchTerm, Map<Integer, Person> persons)
    {
        
        try 
        {
            int count = 0;
            String encode = URLEncoder.encode(searchTerm, "UTF-8"); //encode the data
            URL website = new URL(url+encode);
            InputStream siteIn = website.openStream(); //get the info from site
            JsonReader reader = Json.createReader(siteIn); //Creates a JSON reader from a character stream.
            JsonArray allResults = reader.readArray(); //Returns a JSON array that is represented in the input source. 
            //This method needs to be called only once for a reader instance. 
            for(int i = 0; i < allResults.size(); i++)
            {
                JsonObject obj = allResults.getJsonObject(i);
                JsonObject person = obj.getJsonObject("person");
                JsonValue ratingValue = person.get("id"); 
                // -> Id 
                int id;
                if(ratingValue instanceof JsonNumber)
                {
                    id = ((JsonNumber)ratingValue).intValue();
                }
                else 
                {
                    id = 0;
                }
                if(!persons.containsKey(id)) //extra validation to ensure actors dont get overwritten
                {
                    List<String> images = new ArrayList<>(); // -> Stores images
                    List<String> myComments = new ArrayList<>(); // -> Stores my comments
                    JsonValue objScore = obj.get("score");
                    double score = ((JsonNumber)objScore).doubleValue();// -> Score
                    String name = person.getString("name"); // -> Name
                    // -> ImageUrls
                    String original, medium;
                    if(!obj.getJsonObject("person").isNull("image"))
                    {
                        JsonObject imageUrls = person.getJsonObject("image");
                        medium = imageUrls.getString("medium");
                        original = imageUrls.getString("original");
                    }
                    else
                    {
                        medium = "No medium image";
                        original = "No original image";
                    }
                    images.add(medium);
                    images.add(original);
                    String personLink = person.getString("url"); // -> PersonLink
                    double myRating = 0; // -> MyRating

                    persons.put(id, new Person(score, searchTerm, name, id, images, personLink, myRating, myComments)); // -> Add person to Map
                }
                
                count = printSearchResult(persons, searchTerm, count);
                
                if(count == 0)
                {
                    System.out.println("No results found on Tv Maze, try again.");
                }
                else
                {
                    System.out.println(count + " actors found in Tv Maze API, added to Map...");
                }
                return persons;
            }
        }
        catch (UnsupportedEncodingException ex) 
        {
            Logger.getLogger(OOP_CA2.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (MalformedURLException ex) 
        {
            Logger.getLogger(OOP_CA2.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(OOP_CA2.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Added Sucessfully!");
        return persons;
    }
    
    /*public static void printPersons(Map<Integer, Person> persons)
    {
        for(Map.Entry<Integer, Person> entry: persons.entrySet())
        {
            int key = entry.getKey();
            Person value = entry.getValue();
            System.out.println("Key: " + key + "\n" + value);
        }
    }*/
    
    public static void printPersons(ArrayList<Person> sortArray)
    {
         for (Person a : sortArray) 
            {
                System.out.println(a);
            }
    }
    
    public static ArrayList<Person> sort(ArrayList<Person> personList, String sortingMethod)
    {
        ArrayList<Person> cloneList = clone(personList);
        Comparator c = SortFactory.getComparator(sortingMethod);
        
        Collections.sort(cloneList, c);
        return cloneList;
    }
    
    public static void getAscDesc()
    {
        Scanner in = new Scanner(System.in);
        boolean typeFound = false;
        System.out.println("1: Ascending\n2: Descending");
        int type = in.nextInt();       
        while(!typeFound){
            if(type == 1){
                SortFactory.sortType = SortType.ASCENDING;
                typeFound = true;
            }
            else if (type == 2){
                SortFactory.sortType = SortType.DESCENDING;
                typeFound = true;
            } 
            else
            {
                System.out.println("INVALID INPUT. Try again");
                type = in.nextInt();
            }
        }
    }
    
    public static ArrayList<Person> clone(ArrayList<Person> personList)
    {
        ArrayList<Person> tempList = new ArrayList<Person>();
        for(Person m : personList)
        {
            tempList.add(m.clone());
        }
        return tempList;
    }
    
    public static void editPerson(Map<Integer, Person> persons)
    {
        System.out.println("What person would you like to edit? (by key)");
                        int search = in.nextInt();  
                        in.nextLine();
                        System.out.println(persons.get(search));
                        System.out.println("\nWould you like to add a 'comment' or a 'rating'? ");
                        String searchTerm = in.nextLine();
                        if(searchTerm.equalsIgnoreCase("comment"))
                        {
                            String comment = in.nextLine();
                            /*boolean firstAttempt = true;
                            do
                            {
                                if(!firstAttempt)
                                {
                                    System.out.println("Comment must be between 1-20 characters long.");                                    
                                }
                                System.out.println("\nComment:");
                                comment = in.nextLine();
                                firstAttempt = false;
                            }while(comment.length() <= 20);*/
                            persons.get(search).addComments(comment);
                            
                        }
                        else if(searchTerm.equalsIgnoreCase("rating"))
                        {
                            System.out.println("\nRating:");
                            double rating = in.nextDouble();
                            while(rating < 0 || rating > 10)
                            {
                                System.out.println("Invalid Rating Amount, your rating must be on a scale of 0 - 10");
                                rating = in.nextDouble();
                            }
                            persons.get(search).setMyRating(rating);
                        }
                        System.out.println("\nUpdate Successful!");
    }
    
    
    
    
    public static void writeToHtmlFile(Map<Integer, Person> persons) throws IOException
    {
        BufferedWriter bw = new BufferedWriter(new FileWriter("persons.html"));
        bw.write("<html>\n");
        bw.write("<head>\n<link rel='stylesheet' type='text/css' href='html.css'>\n<title>Exported Html File</title>\n</head>\n");
        bw.write("<body>\n");
        for(Map.Entry<Integer, Person> entry: persons.entrySet())
        {
            bw.write("<div id='main'>\n");
            Person value = entry.getValue(); //get the values of the person
            String name = value.getName();
            bw.write("<h1>" + name + "</h1>\n");
            double score = value.getScore();
            bw.write("Score: " + score + "<br>");
            String queryName = value.getQueryName();
            bw.write("Query Name: " + queryName + "<br>");
            int id = value.getId();
            bw.write("ID: " + id + "<br>");
            bw.write("Images: ");
            List<String> images = value.getImageUrls(); // -> images
            bw.write("<div id='image'><img src='" + images.get(0) + "' alt='image'></div>");
            bw.write("<a href='" + images.get(1) + "'>View original image</a>");
            bw.write("<br>");
            String personLink = value.getPersonLink(); // -> personLink
            bw.write("Person Link: <a href='" + personLink + "'>Click Here</a><br>");
            bw.write("My Comments: ");
            List<String> myComments = value.getMyComments(); // -> myComments
            if(!myComments.isEmpty())
            {
                for (int i = 0; i < myComments.size(); i++) 
                {
                    bw.write("\"" + myComments.get(i) + "\"");
                    if (i < myComments.size()-1)
                    {
                        bw.write(", ");
                    }
                }
            }
            else 
            {
                bw.write("No comments found");
            }
            bw.write("<br>");
            double myRating = value.getMyRating();
            bw.write("My Rating: " + myRating + "<br><br>\n");
            bw.write("</div>\n");
        }
        bw.write("</body>\n");
        bw.write("</html>");
        bw.close();
    }
    
    private static void save(Map<Integer, Person> persons)
    {
        try
        {
            ArrayList<Person> p = convertFileData(persons);
            
            File f = new File("person.dat");
            if(!f.exists())
            {
                f.createNewFile();
            }
            
            DataOutputStream dos = new DataOutputStream(new FileOutputStream(f));
            Iterator<Person> iter = p.iterator();
            while(iter.hasNext())
            {
                Person s = iter.next();
                dos.writeDouble(s.getScore()); // -> score
                dos.writeBytes(pad(s.getQueryName(), 30)); // -> queryName
                dos.writeBytes(pad(s.getName(), 30)); // -> name
                dos.writeInt(s.getId()); // -> id
                List<String> images = s.getImageUrls(); // -> images
                for (String string : images) 
                {
                    dos.writeBytes(pad(string, 80));
                }
                dos.writeBytes(pad(s.getPersonLink(), 50)); // -> personLink
                dos.writeDouble(s.getMyRating()); // -> myRating
                
                List<String> comments = s.getMyComments(); // -> myComments
                int commentCount = comments.size();
                dos.writeInt(commentCount);
                for (String string : comments) 
                {
                    dos.writeBytes(pad(string, 40));
                }
            }
            dos.close();
        } 
        catch (IOException ex) 
        {
            ex.printStackTrace();
        }
    }
    
    public static Map<Integer, Person> load() throws FileNotFoundException, IOException
    {
       File f = new File("person.dat");
       Map<Integer, Person> persons = new HashMap<>();
       if(f.exists())
       {
           DataInputStream dis = new DataInputStream(new FileInputStream(f));
           while(dis.available() > 0)
           {
               double score = dis.readDouble();
               //System.out.println(score);
               
               byte[] queryNameBytes = new byte[30];
               dis.read(queryNameBytes);
               String queryName = removePadding(new String(queryNameBytes));
               //System.out.println(queryName);
               
               byte[] nameBytes = new byte[30];
               dis.read(nameBytes);
               String name = removePadding(new String(nameBytes));
               //System.out.println(name);
               
               int id = dis.readInt();
               //System.out.println(id);
               
               List<String> imageUrls = new ArrayList<>();
               
               byte[] mediumImage = new byte[80];
               dis.read(mediumImage);
               String mImage = removePadding(new String(mediumImage));
               imageUrls.add(mImage);
               
               byte[] originalImage = new byte[80];
               dis.read(originalImage);
               String oImage = removePadding(new String(originalImage));
               imageUrls.add(oImage);
               //System.out.println(imageUrls);
               
               byte[] personLinkBytes = new byte[50];
               dis.read(personLinkBytes);
               String personLink = removePadding(new String(personLinkBytes));
               //System.out.println(personLink);
               
               double myRating = dis.readDouble();
               //System.out.println(myRating);
               
               List<String> myComments = new ArrayList<>();
               
               int commentCount = dis.readInt();
               for(int i = 0; i < commentCount; i++)
               {
                    byte[] myCommentsBytes = new byte[40];
                    dis.read(myCommentsBytes);
                    String myComment = removePadding(new String(myCommentsBytes));
                    myComments.add(myComment);
               }
               
               persons.put(id, new Person(score, queryName, name, id, imageUrls, personLink, myRating, myComments)); // -> Add person to Map               
           }
           dis.close();
       }
       return persons;
    }
    
    public static ArrayList<Person> convertFileData(Map<Integer, Person> persons)
    {
        ArrayList<Person> p = new ArrayList<>();
        for(Map.Entry<Integer, Person> entry: persons.entrySet())
        {
            Person value = entry.getValue();
            p.add(value);
            System.out.println(value);
        }
        return p;
    }
    
    public static String pad(String s, int size)
    {
        while (s.length() < size)
        {
            s = (char)0 + s;
        }
        return s;
    }
    
    public static String removePadding(String s)
    {
        while(s.charAt(0) == (char)0)
        {
            s = s.substring(1);
        }
        return s;
    }
    
}



