
package oop_ca2;

import java.util.ArrayList;
import java.util.List;

/*  
                    •	score (value indicating the quality of match for search query)
                    •	queryName (query name used to retrieve this Actor)
                    •	name
                    •	id
                    •	imageUrls (2 or more)
                    •	personLink
                    •	myRating 
                    •	myComments
*/
public class Person 
{
    private int key;
    private double score;
    private String queryName;
    private String name;
    private int id;
    private List<String> imageUrls;
    private String personLink;
    private double myRating;
    private List<String> myComments;
    
    public Person()
    {
        imageUrls = new ArrayList<>();
    }

    public Person(double score, String queryName, String name, int id, List<String> imageUrls, String personLink, 
            double myRating, List<String> myComments) 
    {
        this.score = score;
        this.queryName = queryName;
        this.name = name;
        this.id = id;
        this.imageUrls = imageUrls;
        this.personLink = personLink;
        this.myRating = myRating;
        this.myComments = myComments;
    }
    


    public double getScore() 
    {
        return score;
    }

    public void setScore(double score) 
    {
        this.score = score;
    }

    public String getQueryName() 
    {
        return queryName;
    }

    public void setQueryName(String queryName) 
    {
        this.queryName = queryName;
    }

    public String getName() 
    {
        return name;
    }

    public void setName(String name) 
    {
        this.name = name;
    }

    public int getId() 
    {
        return id;
    }

    public void setId(int id) 
    {
        this.id = id;
    }

    public List<String> getImageUrls() 
    {
        return imageUrls;
    }

    public void setImageUrls(List<String> imageUrls) 
    {
        this.imageUrls = imageUrls;
    }

    public String getPersonLink() 
    {
        return personLink;
    }

    public void setPersonLink(String personLink) 
    {
        this.personLink = personLink;
    }

    public double getMyRating() 
    {
        return myRating;
    }

    public void setMyRating(double myRating) 
    {
        this.myRating = myRating;
    }

    public List<String> getMyComments() 
    {
        return myComments;
    }

    public void setMyComments(List<String> myComments) 
    {
        this.myComments = myComments;
    }
    public void addComments(String comment) 
    {
        this.myComments.add(comment);
    }
     public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    @Override
    public String toString() 
    {
        String toString = "Score: " + score
                + "\nQueryName: " + queryName 
                + "\nName: " + name 
                + "\nId: " + id 
                + "\nImageUrls: " + imageUrls 
                + "\nPersonLink: " + personLink 
                + "\nMyRating: " + myRating 
                + "\nMyComments: " + myComments 
                + "\n";
                return  toString;
    }
    
    public Person clone()
    {
        Person p = new Person(this.score, this.queryName, this.name,this.id, this.imageUrls, this.personLink, 
            this.myRating, this.myComments);
        return p;
    }
    
    
    
    
}
