/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop_ca2;

import java.util.Comparator;

/**
 *
 * @author D00181907
 */
public class SortFactory 
{
    public static SortType sortType = SortType.ASCENDING;
    
    public static Comparator getComparator(String n)
    {

       if(n.toLowerCase().equals("id"))
       {
            return new IdComparator(sortType);
       }
       else if(n.toLowerCase().equals("rating"))
       {
            return new RatingComparator(sortType);
       }
       else if(n.toLowerCase().equals("score"))
       {
            return new ScoreComparator(sortType);
       }

       return new IdComparator(sortType);
    }
    
}
