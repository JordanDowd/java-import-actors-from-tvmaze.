/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop_ca2;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jordan
 */
public class PersonTest 
{
    
    List<String> myComments;
    
    public PersonTest() 
    {
    }
    
    @BeforeClass
    public static void setUpClass() 
    {
    }
    
    @AfterClass
    public static void tearDownClass() 
    {
    }
    
    @Before
    public void setUp() 
    {
        myComments = new ArrayList<>();
    }
    
    @After
    public void tearDown() 
    {
    }

    @Test
    public void testAddComments() 
    {
        assertEquals("Wrong initial size", 0, myComments.size());
        myComments.add("Testing");
        assertEquals("Wrong initial size", 1, myComments.size());
    }
    
    @Test
    public void testToString() 
    {
    }
    
    @Test
    public void testClone() 
    {
    }
    
}
