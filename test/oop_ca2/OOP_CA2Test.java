/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop_ca2;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author User
 */
public class OOP_CA2Test 
{
    
    public OOP_CA2Test() 
    {
    }
    

  @Test
  public void testSortById()
  {
      SortFactory.sortType = SortType.ASCENDING;
      ArrayList<Person> testArray = new ArrayList<>();
      List<String> images = new ArrayList<>();
      List<String> comments = new ArrayList<>();

      //3 sample actors with different names

                          //score, queryName,       name,      id, imageUrls,               personLink,                     myRating, myComments
      Person a = new Person(74, "ben Stiller", "Ben Stiller", 4005,images, "http://www.tvmaze.com/people/39582/ben-stiller", 0.0, comments);
      Person b = new Person(50, "adam sandler", "Adam Sandler", 6541,images, "http://www.tvmaze.com/people/39582/adam-sandler", 0.0, comments);
      Person c = new Person(84, "James Franco", "James Franco", 2381,images, "http://www.tvmaze.com/people/39582/James-Defranco", 0.0, comments);
      
      // add the people in the same order
      testArray.add(a);
      testArray.add(b);
      testArray.add(c);
      // expected outcome:(defaults to ascending order
      //1. James Franco -> 2381
      //2. Ben Stiller  -> 4005
      //3. Adam Sandler -> 6541
      
      
      ArrayList<Person> sortedArray = OOP_CA2.sort(testArray, "id");
      
      assertEquals( "James Franco", sortedArray.get(0).getName());
      assertEquals( 2381, sortedArray.get(0).getId());
      
      assertEquals( "Ben Stiller", sortedArray.get(1).getName());
      assertEquals( 4005, sortedArray.get(1).getId());
      
      assertEquals( "Adam Sandler", sortedArray.get(2).getName());
      assertEquals( 6541, sortedArray.get(2).getId());
  }
  
  @Test
  public void testSortByRating()
  {
      SortFactory.sortType = SortType.ASCENDING;
      ArrayList<Person> testArray = new ArrayList<>();
      List<String> images = new ArrayList<>();
      List<String> comments = new ArrayList<>();

      //3 sample actors with different names

                          //score, queryName,       name,      id, imageUrls,               personLink,                     myRating, myComments
      Person a = new Person(74, "ben Stiller", "Ben Stiller", 4005,images, "http://www.tvmaze.com/people/39582/ben-stiller", 4.2, comments);
      Person b = new Person(50, "adam sandler", "Adam Sandler", 6541,images, "http://www.tvmaze.com/people/39582/adam-sandler", 4, comments);
      Person c = new Person(84, "James Franco", "James Franco", 2381,images, "http://www.tvmaze.com/people/39582/James-Defranco", 5, comments);
      
      // add the people in the same order
      testArray.add(a);
      testArray.add(b);
      testArray.add(c);
      // expected outcome:(defaults to ascending order
      //1. Adam Sandler -> 4
      //2. Ben Stiller  -> 4.2
      //3. James Franco -> 5
      
      
      ArrayList<Person> sortedArray = OOP_CA2.sort(testArray, "rating");
      
      assertEquals( "Adam Sandler", sortedArray.get(0).getName());
      assertEquals( 4, sortedArray.get(0).getMyRating(), 0);
      
      assertEquals( "Ben Stiller", sortedArray.get(1).getName());
      assertEquals( 4.2, sortedArray.get(1).getMyRating(), 0);
      
      assertEquals( "James Franco", sortedArray.get(2).getName());
      assertEquals( 5, sortedArray.get(2).getMyRating(), 0);
  }
  @Test
  public void testSortByIdDescending()
  {
      
      
      //sets the sort type to descending
      SortFactory.sortType = SortType.DESCENDING;
      ArrayList<Person> testArray = new ArrayList<>();
      List<String> images = new ArrayList<>();
      List<String> comments = new ArrayList<>();

      //3 sample actors with different names

                          //score, queryName,       name,      id, imageUrls,               personLink,                     myRating, myComments
      Person a = new Person(74, "ben Stiller", "Ben Stiller", 4005,images, "http://www.tvmaze.com/people/39582/ben-stiller", 0.0, comments);
      Person b = new Person(50, "adam sandler", "Adam Sandler", 6541,images, "http://www.tvmaze.com/people/39582/adam-sandler", 0.0, comments);
      Person c = new Person(84, "James Franco", "James Franco", 2381,images, "http://www.tvmaze.com/people/39582/James-Defranco", 0.0, comments);
      
      // add the people in the same order
      testArray.add(a);
      testArray.add(b);
      testArray.add(c);
      // expected outcome:(Descending)
      //1. Adam Sandler -> 6541
      //2. Ben Stiller  -> 4005
      //3. James Franco -> 2381
      
      
      ArrayList<Person> sortedArray = OOP_CA2.sort(testArray, "id");
      assertEquals( "Adam Sandler", sortedArray.get(0).getName());
      assertEquals( 6541, sortedArray.get(0).getId());
      
      assertEquals( "Ben Stiller", sortedArray.get(1).getName());
      assertEquals( 4005, sortedArray.get(1).getId());
      
      assertEquals( "James Franco", sortedArray.get(2).getName());
      assertEquals( 2381, sortedArray.get(2).getId());
  }
  
  @Test
  public void testClone()
  {
      //testing to see if i am creating a shallow copy or a deep copy
      
      ArrayList<Person> testArray = new ArrayList<>();
      List<String> images = new ArrayList<>();
      List<String> comments = new ArrayList<>();

      //3 sample actors with different names

                          //score, queryName,       name,      id, imageUrls,               personLink,                     myRating, myComments
      Person a = new Person(74, "ben Stiller", "Ben Stiller", 4005,images, "http://www.tvmaze.com/people/39582/ben-stiller", 0.0, comments);
      Person b = new Person(50, "adam sandler", "Adam Sandler", 6541,images, "http://www.tvmaze.com/people/39582/adam-sandler", 0.0, comments);
      Person c = new Person(84, "James Franco", "James Franco", 2381,images, "http://www.tvmaze.com/people/39582/James-Defranco", 0.0, comments);
      
      // add the people
      testArray.add(a);
      testArray.add(b);
      testArray.add(c);
      ArrayList<Person> deepCopyArray = OOP_CA2.clone(testArray);
      
      testArray.get(0).setId(3000);
      
      //checking if a change on an element in testArray will affect the deep copy array
      assertEquals(4005, deepCopyArray.get(0).getId());
      
      testArray.remove(0);
      for (Person p : deepCopyArray) 
            {
                System.out.println(p);
            }
     
      //checking if the delete on test array affected the size of the deep copy array
      assertEquals( 3, deepCopyArray.size());
      
            
      
  }
  
  
}
